FROM python:3.12
RUN apt update
RUN apt install build-essential

RUN mkdir -p /var/www/html
WORKDIR /var/www/html

COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

CMD ["python", "main.py"]